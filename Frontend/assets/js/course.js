//grabbing course ID to the URL
console.log(window.location.search);

// instantiate a URLSearchParams object

let params = new URLSearchParams(window.location.search);

console.log(params);
console.log(params.has('courseId'));
console.log(params.get('courseId'));

let courseId= params.get('courseId');
let userId = localStorage.getItem("id")
console.log(userId);
let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector('#enrollContainer')
let authToken = localStorage.getItem('token');
let table = document.querySelector('#enrolleesTable')

let admin = localStorage.getItem("isAdmin");
console.log(admin);

let eTable;

fetch(`http://localhost:4000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
console.log(data);
  

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;

  if(admin === "false" || !admin) {
	   enrollContainer.innerHTML =
	     `
      <button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
          
       `
     //data enrollees could appear
    //students = data.enrollees.map(enrollees =>{
    document.querySelector("#enrollButton").addEventListener("click", () => {
	  
	  fetch('http://localhost:4000/api/users/enroll', {
      method: 'POST',
      headers: {
       'Content-Type': 'application/json',
       'Authorization': `Bearer ${authToken}`
       
      },
         body:JSON.stringify({
       	 courseId:courseId
       })
     })

     .then(res => res.json())
     .then(data => {
     	 console.log(data);
       console.log(enrollees._id);

       
       /*if enrollees.enrollees includes(userId){
        alert('You have already enrolled in this class')
        window.location.replace("courses.html")
       }*/
         

     	  if(data === true){
     		   alert("Thank you for enrolling! See you in class!")
     		   window.location.replace("courses.html");
     	  }else {
     		   alert("something went wrong") 


          }

        //  })
        })

       })
// data.enrollees could appear

     }
   })

// ACCESSING COURSES BY COURSE ID
  fetch(`http://localhost:4000/api/courses/${courseId}`)
  .then(res => res.json())
  .then(data => {
    console.log(data);
    console.log(data.enrollees)
    if(data.enrollees != undefined && data.enrollees.length > 0 ){
      console.log(data.enrollees.length)

    // to access data.enrollees for the user id
    enrollData = data.enrollees.map(users => {
      
   // MATCHING COURSE ID WITH NAMES
     fetch(`http://localhost:4000/api/users/${users.userId}`)
      .then(res => res.json())
      .then(userdata => {
      console.log(userdata);
      console.log(admin); 
      if(admin === "true") {
        let Users = data.enrollees
        const row = document.createElement('tr');
        
        //tried changing the date format
        //users.enrolledOn.toISOString().substring(0, 10);
        row.innerHTML =
      
       `
       <tr>
       <td>${userdata.firstName} ${userdata.lastName}</td>
       <td>${users.enrolledOn}</td>
       </tr>
      `


       table.appendChild(row);
    
       console.log(users.userId)
       console.log(users.enrolledOn)
       }else{
  // remove course history table
      $('#enrolleesTable').remove();
       }
      })
    })
    }else {
      $('#enrolleesTable').remove();
     
    }  
  })

  

    





